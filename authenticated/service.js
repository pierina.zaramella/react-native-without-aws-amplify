import Utf8 from 'crypto-js/enc-utf8';
import hmacSHA256 from 'crypto-js/hmac-sha256';
import Base64 from 'crypto-js/enc-base64';

const GetSecretHash = (username, appClientId, appSecretKey) => {
    var dataString = username + appClientId;

    var data = Utf8.stringify(dataString);
    var key = Utf8.stringify(appSecretKey);

    return Base64.stringify(hmacSHA256(data, key));
}

export { GetSecretHash }