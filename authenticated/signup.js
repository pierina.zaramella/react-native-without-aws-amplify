import React from 'react'

import {
    CognitoUserPool,
    CognitoUserAttribute
} from 'amazon-cognito-identity-js';

import { GetSecretHash } from '../authenticated/service'

const REACT_APP_SECRET = "1pqdbg2fchor4v9180tm6k62rjuph9k8qloplrrec5r74h7t0cb5",
    REACT_APP_REGION = "us-east-1",
    REACT_APP_CLIENT_ID = "1ic5clp0s6edh07513lip7p21p",
    REACT_APP_USER_POOL = "us-east-1_K6U6Uvmc9";

const Signup = (user) => {
    var poolData = {
        UserPoolId: REACT_APP_USER_POOL, // Your user pool id here
        ClientId: REACT_APP_CLIENT_ID, // Your client id here
    };
    const userPool = new CognitoUserPool(poolData);

    var dataEmail = {
        Name: 'email',
        Value: 'email@mydomain.com',
    };

    var dataPhoneNumber = {
        Name: 'phone_number',
        Value: '+15555555555',
    };

    var dataName = {
        Name: 'name',
        Value: 'Julieta',
    };

    var dataFamilyName = {
        Name: 'family_name',
        Value: 'Venegas',
    };

    const attributeList = []
    const username = 'prueba'
    const hash = GetSecretHash(username, REACT_APP_CLIENT_ID, REACT_APP_SECRET)
    var attributeEmail = new CognitoUserAttribute(dataEmail);
    var attributePhoneNumber = new CognitoUserAttribute(
        dataPhoneNumber
    );

    attributeList.push(attributeEmail);
    attributeList.push(attributePhoneNumber);
    attributeList.push(dataName);
    attributeList.push(dataFamilyName);
    userPool.signUp(username, 'password', attributeList, null, function (
        err,
        result
    ) {
        if (err) {
            alert(err.message || JSON.stringify(err));
            return;
        }
        const cognitoUser = result.user;
        console.log('user name is ' + cognitoUser.getUsername());
    });
}

export default Signup